﻿namespace Idle
{
    public class CityUpgrade : Upgrade
    {
		//Base method is responsible for the purchase of the upgrade
		public override void UpgradeItem(int id)
        {
            base.UpgradeItem(id);
            if (!priced)
                return;
			//items = DataManager.cityUpgradeData;
			/////////////////////////////////////////////// #Start
			//This is where the logic of the action is written, provided that we have purchased

			DataManager.data.MoneyByClick += items[id].value; //Adds Value to money by click
			items[id].value += 1;
			/////////////////////////////////////////////// #End
			DataManager.cityUpgradeData[id] = items[id].value;
			Upgraded();//The method is responsible for the successful purchase, makes saving
        }

    }
}