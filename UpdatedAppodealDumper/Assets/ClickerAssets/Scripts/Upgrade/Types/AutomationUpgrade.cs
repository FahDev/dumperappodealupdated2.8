﻿
namespace Idle
{
    public class AutomationUpgrade : Upgrade 
    {
		//Base method is responsible for the purchase of the upgrade
		public override void UpgradeItem(int id)
        {
            base.UpgradeItem(id);
            if (!priced)
                return;
			/////////////////////////////////////////////// #Start
			//This is where the logic of the action is written, provided that we have purchased
			DataManager.data.MoneyPerSecond += items[id].value; //Adds Value to Money Per Second
			switch (id)
			{
				case 0:
					items[id].value++;
					break;
				case 1:
					items[id].value += 5;
					break;
				case 2:
					items[id].value += 50;
					break;
				case 3:
					items[id].value += 500;
					break;
			}
			/////////////////////////////////////////////// #End
			DataManager.automationUpgradeData[id] = items[id].value;
			Upgraded(); //The method is responsible for the successful purchase, makes saving

        }


    }
}