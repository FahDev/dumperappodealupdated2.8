﻿using UnityEngine;
using System.Collections.Generic;

namespace Idle
{
    public class DataManager : MonoBehaviour
    {
        //Components
        static DataSaver dataSaver;

        public static Data data;
        public static UpgradeData upgradeData;
		public static List<int> cityUpgradeData;
		public static List<int> automationUpgradeData;

		private void Awake()
        {
            Init();
        }

        void Init()
        {
			//Create an instance of classes
			cityUpgradeData = new List<int>();
			List<Item> cityUpgrade = GameObject.Find("CityScreen").GetComponent<CityUpgrade>().items;
			foreach (Item item in cityUpgrade)
			{
				cityUpgradeData.Add(item.value);
			}
			automationUpgradeData = new List<int>();
			List<Item> automationUpgrade = GameObject.Find("AutomationScreen").GetComponent<AutomationUpgrade>().items;
			foreach (Item item in automationUpgrade)
			{
				automationUpgradeData.Add(item.value);
			}
			data = new Data();
            upgradeData = new UpgradeData();

            //Cached component from our Gameobject
            dataSaver = GetComponent<DataSaver>();

            LoadData();

            //We make the first launch, and give the starter kit
            if (!data.isFirstStartComplete)
            {
                data.isFirstStartComplete = true;

                data.Money = 100;
                data.MoneyByClick = 1;

                SaveData();
            }
			int i = 0;
			foreach (Item item in cityUpgrade)
			{
				item.value = cityUpgradeData[i];
				i++;
			}
			i = 0;
			foreach (Item item in automationUpgrade)
			{
				item.value = automationUpgradeData[i];
				i++;
			}
		}

        //Static save method, can be called from any class "DataManager.SaveData();"
        public static void SaveData()
        {
            object[] obj = new object[4]; // Create a local variable for an array of objects
            //register the objects we need
            obj[0] = data;
            obj[1] = upgradeData;
			obj[2] = cityUpgradeData;
			obj[3] = automationUpgradeData;
			dataSaver.Save(obj); //Save to DataSaver
        }

        //Static load method, can be called from any class "DataManager.LoadData();"
        public static void LoadData()
        {
            object[] obj = dataSaver.Load();  //Get the data array from the file
            //We assign the objects we need
            if (obj != null)
            {
                data = obj[0] as Data;
                upgradeData = obj[1] as UpgradeData;
				cityUpgradeData = obj[2] as List<int>;
				automationUpgradeData = obj[3] as List<int>;
			}

        }

    }

}
